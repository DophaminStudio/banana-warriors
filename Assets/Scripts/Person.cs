﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Person : MonoBehaviour {
    public GameObject UIPrefab;
    //public Transform transformPerson;
    public Transform spawnParent;
	// Use this for initialization
	void Start () {
        SpawnUI();
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    void SpawnUI()
    {
        GameObject clone = Instantiate(UIPrefab,new Vector3(transform.localPosition.x, spawnParent.transform.localPosition.y,spawnParent.transform.localPosition.z), Quaternion.identity,spawnParent);

    }
}
