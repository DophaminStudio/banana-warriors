﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IUnit {
    UnitType getUnitType();

    int getHealth();
    void setHealth(int health);

    int getActualAbility();
    void setActualAbility(int abilityNum);
    string getAbilityName(int abilityNum);

    bool isAlive();
    void setAlive(bool alive);

    void resetState();
}
