﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShamanMonkey : IUnit {
    public static string BASE_ATTACK_NAME = "base attack";  // TODO определиться с названием
    public static string ALT_ATTACK_NAME = "I'm hungry";

    public static int BASE_ATTACK = 0;
    public static int ALT_ATTACK = 1;

    public int health = 3;
    public bool alive = true;

    public int actualAbility = 0;

    // для дефолтного шамана
    public ShamanMonkey() { }

    // конструктор для создания кастомного состояния
    public ShamanMonkey(int health, bool alive, int actualAbility) {
        this.health = health;
        this.alive = alive;
        this.actualAbility = actualAbility;
    }

    public string getAbilityName(int abilityNum) {
        return abilityNum == 0 ? BASE_ATTACK_NAME : ALT_ATTACK_NAME;
    }

    public int getActualAbility() {
        return actualAbility;
    }

    public int getHealth() {
        return health;
    }

    public void resetState() {  // для сброса абилок между ходами
        actualAbility = BASE_ATTACK;
    }

    public void setActualAbility(int abilityNum) {
        if (abilityNum != BASE_ATTACK || abilityNum != ALT_ATTACK) {
            throw new System.Exception("unappropriate ability number chosen");
        }

        actualAbility = abilityNum;
    }

    public void setAlive(bool alive) {
        this.alive = alive;
    }

    public void setHealth(int health) {
        this.health = health;
    }

    public bool isAlive() {
        return alive;
    }

    public UnitType getUnitType() {
        return UnitType.ShamanMonkey;
    }
}
