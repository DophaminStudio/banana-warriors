﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class LoadingScreen : MonoBehaviour {

	public static LoadingScreen Instance;

	public bool dontDestroy = false;

	public GameObject loadingScreen;
	public Text loadingLabel;

	public string labelPerfix = "Loading";
	public float labelUpdateTime = 1;

	public bool isShown = false, showOnLoading = true;

	private float labelUpdateTimer = 0;
	private int currentPoints = 0, maxPoints = 3;

	void Awake(){
		if(dontDestroy){
			DontDestroyOnLoad(gameObject);
			if(LoadingScreen.Instance != null){
				Destroy(gameObject);
			}else{
				Instance = this;
			}
		}else{
			Instance = this;
		}

        if (Application.systemLanguage == SystemLanguage.Russian)
        {
            labelPerfix = "Загрузка";
        }
        else
        {
            labelPerfix = "Loading";
        }

        //loadingScreen.SetActive(false);
		loadingLabel.text = labelPerfix;
	}
	
	void Update(){
		//hide the loading screen if the scene is loaded
	    if (Application.isLoadingLevel && !isShown && showOnLoading) {
			Show();
            showOnLoading = false;
        }
        else if(!Application.isLoadingLevel && isShown){
			Hide();
		}
	}

	//function to enable the loading screen
	public void Show(){
		//enable the loading image object 
		loadingScreen.SetActive(true);
		isShown = true;
		loadingLabel.text = labelPerfix;
	}

	//function to hide the loading screen
	public void Hide(){
		loadingScreen.SetActive(false);
		isShown = false;
		labelUpdateTimer = 0;
		loadingLabel.text = labelPerfix;
		currentPoints = 0;
		StopCoroutine ("LabelUpdate");
	}

	public IEnumerator LabelUpdate(){
		yield return new WaitForSecondsRealtime (labelUpdateTime);
		
		currentPoints++;
		
		if(currentPoints > maxPoints){
			currentPoints = 0;
			loadingLabel.text = labelPerfix;
		}else{
			loadingLabel.text += ".";
		}

		StartCoroutine ("LabelUpdate");
	}
}