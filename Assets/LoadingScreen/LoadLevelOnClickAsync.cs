using UnityEngine;
using System.Collections;

[AddComponentMenu("NGUI/Examples/Load Level On Click Async")]
public class LoadLevelOnClickAsync : MonoBehaviour
{
	public string levelName;

	void OnClick ()
	{
		if (!string.IsNullOrEmpty(levelName))
		{
			StartCoroutine(LoadNewLevel());
		}
	}

	IEnumerator LoadNewLevel(){
		AsyncOperation async = Application.LoadLevelAsync(levelName);

		if(LoadingScreen.Instance){
			StartCoroutine(LoadingScreen.Instance.LabelUpdate());
			yield return async;
		}
	}
}